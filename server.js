const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config();
const cors = require('cors');
const app = express();
const session = require('express-session');
const MongoStore = require('connect-mongo');

const apiUserRoutes = require('./routes/api-user-rotes');
const apiCalculationRouter = require('./routes/api-calculation-routes');

var corsOptions = {
  origin: ['http://localhost:3000', 'https://calc-client-one.vercel.app'],
  optionsSuccessStatus: 200,
  methods: ['POST', 'PUT', 'GET', 'OPTIONS', 'HEAD'],
  credentials: true
}

mongoose
  .connect(process.env.MONGO_URL)
  .then((res) => console.log('Connected to DB'))
  .catch((err) => connsole.log(`Error connection to DB: ${err}`));

app.listen(process.env.PORT, (error) => {
  error ? console.log('Error connection') : console.log(`Connected to ${process.env.PORT} port`); 
});

app.use(session({
  secret: 'my-secret-key',
  resave: false,
  saveUninitialized: true,
  store: MongoStore.create({ mongoUrl: process.env.MONGO_URL, collection: 'sessions' }),
  cookie: { 
    SameSite: 'none',
    httpOnly: true,
    secure: true,
    maxAge: 1000 * 60 * 60 *24
  }
}));

app.use(cors(corsOptions));
app.use(express.urlencoded({ extendes: false }));
app.use(express.json());

app.use(apiUserRoutes);
app.use(apiCalculationRouter);