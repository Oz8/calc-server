const express = require('express');
const router = express.Router();
const { getUser, signUp, login, logOut } = require('../controller/api-user-controller');

router.get('/get-user', getUser);
router.post('/sign-up', signUp);
router.post('/login', login);
router.post('/logout', logOut);

module.exports = router;