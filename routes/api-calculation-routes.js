const express = require('express');
const router = express.Router();
const { getCalculations, addCalculation } = require('../controller/api-calculation-controller');

router.post('/get-calculations', getCalculations);
router.post('/add-calculation', addCalculation);

module.exports = router;
