const Calculation = require('../models/calculation');

const handleError = (res, error) => {
  res.status(500).send(error.message);
}

const getCalculations = (req, res) => {
  const { userID } = req.body;

  Calculation
    .find().where('userID', userID).sort({ $natural: -1 }).limit(15)
    .then((calculations) => {
      res
      .status(200)
      .json(calculations);
    })
    .catch((error) => handleError(res, error));
};

const addCalculation = (req, res) => {
  const { userID, equation, result } = req.body;

  const newCalculation = new Calculation({
    userID,
    equation,
    result
  });

  newCalculation
    .save()
    .then((calculation) => {
      res
      .status(200)
      .json(calculation)
      })
    .catch((error) => handleError(res, error));
}

module.exports = { 
  getCalculations, 
  addCalculation 
};