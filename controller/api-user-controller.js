const User = require('../models/user');

const handleError = (res, error) => {
  res.status(500).send(error.message);
}

const getUser = (req, res) => {
  if(req.session.userId) {
    User
      .findById(req.session.userId)
      .then(() => {
        res
          .status(200)
          .json({'logged': true, 'userID': req.session.userId, 'sessionID': req.sessionID});
      })
      .catch((error) => handleError(res, error)); 
  } else {
    res.json({'logged': false, 'userID': req.session.userId || null});
  }
};

const signUp = (req, res) => {
  const { name, password } = req.body;

  const newUser = new User ({
    name,
    password
  });

  User.exists({name: newUser.name})
    .then(result => { 
      if(result === null) {
        newUser
          .save()
          .then((user) => {
            req.session.userId = user._id;
            res
              .status(200)
              .json({'logged': 'true'})})
          .catch((error) => handleError(res, error));
      } else {
        res
          .status(200)
          .json({errorMessage: `Username already taken`});
      }
  });
}

const login = (req, res) => {
  const { name, password } = req.body;

  User
  .exists({name})
  .then((result) => {
    if(result !== null) {
      User
        .findOne().where('name', name)
        .then((user) => {
          if(user.password == password) {
            req.session.userId = result._id;
            res
              .status(200)
              .json({'logged': true, 'userID': req.session.userId, 'sessionID': req.sessionID});
          } else {
            res.status(200).json({errorMessage: 'Incorrect password'});
          }
        })
    } else {
      res.status(200).json({errorMessage: 'Username not found'});
    }
  })
  .catch((error) => handleError(res, error));
}

const logOut = (req, res) => {
  // if(req.session.userId) {

  // } else {
  //   res.json({'logged': false});
  // }
  req.session.destroy(function(err) {
    res
      .clearCookie('connect.sid', { path: '/' })
      .json({'message': 'user logged out'});
  })
};


module.exports = { 
  getUser, 
  signUp, 
  login,
  logOut
};
