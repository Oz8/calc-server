const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const calculationSchema = new Schema({
  userID: {
    type: String,
    require: true
  },
  equation: {
    type: String,
    required: true
  },
  result: {
    type: String,
    required: true
  }
});

const Calculation = mongoose.model('Calculation', calculationSchema)

module.exports = Calculation;